status: pre-alpha

badges: [![Gem Version](https://fury-badge.herokuapp.com/rb/planter.png)](http://badge.fury.io/rb/planter)[![Code Climate](https://codeclimate.com/github/philoserf/planter.png)](https://codeclimate.com/github/philoserf/planter)[![Test Coverage](https://codeclimate.com/github/philoserf/planter/badges/coverage.svg)](https://codeclimate.com/github/philoserf/planter)[![Dependency Status](https://gemnasium.com/philoserf/planter.png)](https://gemnasium.com/philoserf/planter)[![Build Status](https://travis-ci.org/philoserf/planter.svg?branch=master)](https://travis-ci.org/philoserf/planter)[![Stories in Ready](https://badge.waffle.io/philoserf/planter.png?label=ready&title=Ready)](https://waffle.io/philoserf/planter)

# Planter

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'planter'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install planter

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it ( https://github.com/[my-github-username]/planter/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
