SimpleCov.start do
  minimum_coverage = 90
  maximum_coverage_drop = 2
end
