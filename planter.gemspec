# coding: utf-8

lib = File.expand_path('../lib', __FILE__)

$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'planter/version'

Gem::Specification.new do |spec|
  spec.name = 'planter'
  spec.version = Planter::VERSION
  spec.authors = ['Mark Ayers']
  spec.email = ['mark@philoserf.com']
  spec.summary = 'Planter: A project skeleton generator.'
  spec.description = 'Planter creates a base project folder/file structure' \
    'based upon a configurable skeleton.'
  spec.homepage = ''
  spec.license = 'MIT'

  spec.files = `git ls-files -z`.split("\x0")
  spec.executables = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.7'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'cucumber'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'wwtd'
  spec.add_development_dependency 'codeclimate-test-reporter'
end
