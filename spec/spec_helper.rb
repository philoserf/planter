require 'codacy-coverage'
Codacy::Reporter.start

$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'planter'
require 'simplecov'
