Feature: Planter
    As a maker of software and other folder/file based projects
    I want a tool to automate the creation of project skeletons
    So that creating project folders and files is quick and consistent.

    Scenario: Usage
        When I execute the command planter --help
        Then I see the usage instructions in STDOUT
